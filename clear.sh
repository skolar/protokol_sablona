#!/bin/bash
echo > smaz
ls | grep '\~' >> smaz
ls | grep 'aux$' >> smaz
ls | grep 'bcf$' >> smaz
ls | grep 'bbl$' >> smaz
ls | grep 'blg$' >> smaz
ls | grep 'log$' >> smaz
ls | grep 'out$' >> smaz

ls | grep 'run.xml$' >> smaz
ls | grep 'gnuplot$' >> smaz
ls | grep 'gnuplottex' >> smaz
ls | grep 'synctex.gz$' >> smaz


sed -i '/fit\.log/d' smaz

sed -i 's/^/rm "/;s/$/"/' smaz
sed -i '/rm ""/d' smaz
chmod +x smaz
./smaz
rm smaz
